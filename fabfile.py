from fabric import task
from os.path import exists, join
from os import makedirs


def build(conn, folder, srcfile):
    if not exists("./build"):
        makedirs("./build")
    conn.run(
        """\
        cd %s && xelatex \
            --output-directory=../build/ %s
        """ % (folder, srcfile),
        replace_env=False
    )


@task
def build_cv(conn):
    """Builds CV (output to ./build/ folder)"""
    build(conn, "cv", "resume_cv.tex")


@task
def build_cover(conn):
    """Builds Cover (output to ./build/ folder)"""
    build(conn, "cv", "cover_letter.tex")


@task
def lint(conn):
    """Lint latex documents with lacheck"""
    to_lints = ["cover_letter.tex", "resume_cv.tex"]
    for to_lint in to_lints:
        print("Linting %s" % to_lint)
        print("-------------------------")
        conn.run(
            "cd cv && lacheck %s" % (to_lint),
            replace_env=False
        )
        print("\n\n")
